<?php

add_action('wp_enqueue_scripts', 'theme_enqueue_styles');
function theme_enqueue_styles()
{
    wp_enqueue_script(
        'child-script',
        get_stylesheet_directory_uri() . '/build/script.js',
        null,
        microtime()
    );
    wp_enqueue_style(
        'parent-style',
        get_template_directory_uri() . '/style.css'
    );
    wp_enqueue_style(
        'google-fonts',
        'https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,700,800'
    );
    wp_enqueue_style('child-style',
        get_stylesheet_directory_uri() . '/build/style.css',
        array('parent-style'),
        WP_DEBUG ? microtime() : '1.5'
    );

    wp_enqueue_script(
        'child-script',
        get_stylesheet_directory_uri() . '/build/script.js',
        null,
        WP_DEBUG ? microtime() : '1.3'
    );

    wp_localize_script(
        'child-script',
        'timesheetsForOutlook',
        [
            'downloadUrl' => home_url('/download'),
            'methodUrl'   => home_url('/metodyka-pracy'),
        ]
    );
}

add_shortcode('tfo_downloads', 'tfo_download_form');
// For reasons completely unknown, the form normally gets rendered twice...
$download_form_been_called = false;
function tfo_download_form()
{
    global $download_form_been_called;
    if ($download_form_been_called) {
        return;
    }
    $download_form_been_called = true;
    ?>
    <form class="downloadForm js-downloadForm">
        <label>
            <span class="sr-only">Hasło</span>
            <input type="password" name="password" placeholder="Hasło" autocomplete="cc-password">
        </label>
        <button type="submit" class="et_pb_button btn-secondary">Pobierz</button>
        <output class="downloadForm__status js-downloadForm__status"></output>
    </form>
    <?php
}

add_action('acf/init', function () {
    acf_add_options_page();
});

add_filter('acf/update_value/type=password', function ($value) {
    return password_hash($value, PASSWORD_DEFAULT);
}, 10, 1);

add_action('init', 'handle_download_request');

function handle_download_request()
{
    if ($_SERVER['REQUEST_URI'] === '/download') {
        $receivedPassword = $_POST['password'];

        $correctPasswordHash = get_field('tfo_download_password', 'option');

        if (password_verify($receivedPassword, $correctPasswordHash)) {
            /** @var WP_Post $file */
            $file = get_field('tfo_download_file', 'option');
            send_file(get_attached_file($file['id']));
            exit;
        } else {
            http_response_code(403);
            exit();
        }
    }
}

function send_file($filename)
{
    if (file_exists($filename)) {
        header('Content-Description: File Transfer');
        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment; filename="' . basename($filename) . '"');
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        header('Content-Length: ' . filesize($filename));
        readfile($filename);
        exit;
    }
}

add_action('et_after_main_content', 'contact_form_icon');
function contact_form_icon()
{
    if (class_exists('WPCF7') && ! is_page('kontakt')) {
        ob_start();
        ?>
        <div class="tfo_contactWidget" id="tfo_contactWidget">
            <a
                href="#"
                class="tfo_contactWidget__icon tfo_contactIcon"
                data-toggle-target="#tfo_contactWidget"
                data-toggle-visible-class="tfo_contactWidget--active"
                data-toggle-focus-target="#tfo_contactForm__firstInput"
                data-toggle-blur-target="#tfo_contactWidget"
            >
                <span class="dashicons dashicons-format-chat"></span>
            </a>
            <div class="tfo_contactWidget__content">
                [contact-form-7 id="92" title="Napisz do nas"]
            </div>
        </div>
        <?php
        echo do_shortcode(ob_get_clean());
    }
}
