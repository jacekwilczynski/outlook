document.addEventListener('DOMContentLoaded', function() {
  const options = document.querySelectorAll('#order-form select option');
  Array.prototype.forEach.call(options, function(option) {
    if (option.innerText.startsWith('*')) {
      const cleanValue = option.value.slice(1);
      option.setAttribute('selected', true);
      option.value = cleanValue;
      option.innerText = cleanValue;
    }
  });
});
