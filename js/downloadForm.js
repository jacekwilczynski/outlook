import download from 'js-file-download';

document.addEventListener('DOMContentLoaded', function() {
  const url = window.timesheetsForOutlook.downloadUrl;

  const forms = document.getElementsByClassName('js-downloadForm');

  Array.prototype.forEach.call(forms, function(form) {
    form.addEventListener('submit', function(event) {
      event.preventDefault();

      const form = this;
      const statusOutput = form.querySelector('.js-downloadForm__status');
      showStatus('wait');

      const xhr = new XMLHttpRequest();
      xhr.open('POST', url, true);
      xhr.responseType = 'blob';
      xhr.onload = function() {
        if (this.status === 200) {
          showStatus('success');
          const blob = this.response;
          download(blob, 'Timesheets For Outlook.zip');
        } else {
          showStatus('error');
        }
      };
      xhr.onerror = showStatus.bind(null, 'error');
      xhr.send(new FormData(form));
      form.reset();

      function showStatus(status) {
        form.classList.toggle('downloadForm--success', status === 'success');
        form.classList.toggle('downloadForm--error', status === 'error');
        statusOutput.innerHTML =
          status === 'success'
            ? `Dziękujemy za pobranie wtyczki <b>Timesheets For Outlook</b>. Jeśli jeszcze tego nie zrobiłeś, zachęcamy do zapoznania się z <a href="${
                window.timesheetsForOutlook.methodUrl
              }" target="_blank" rel="noopener noreferrer">metodyką pracy</a>.`
            : status === 'error'
            ? 'Nieprawidłowe hasło.'
            : '<div class="lds-spinner"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>';
      }
    });
  });
});
