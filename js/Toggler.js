function Toggler({ element, target, visibleClass, focusTarget, blurTarget }) {
  let isVisible = false;
  let clickedOnTarget = false;

  function activate() {
    element.addEventListener('click', function(event) {
      event.preventDefault();
      toggle();
    });
    if (blurTarget) {
      activateBlur();
    }
  }

  function activateBlur() {
    blurTarget.addEventListener('click', function() {
      clickedOnTarget = true;
    });
    window.addEventListener(
      'click',
      function() {
        setTimeout(function() {
          if (clickedOnTarget) {
            clickedOnTarget = false;
          } else {
            toggle(false);
          }
        });
      },
      true
    );
  }

  function toggle(newState = !isVisible) {
    isVisible = newState;
    target.classList.toggle(visibleClass, isVisible);
    if (focusTarget && isVisible) {
      focusTarget.focus();
    }
    clickedOnTarget = true;
    setTimeout(function() {
      clickedOnTarget = false;
    });
  }

  return { activate, toggle };
}

export default Toggler;
