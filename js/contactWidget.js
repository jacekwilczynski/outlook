import Toggler from './Toggler';

let toggler;

document.addEventListener('DOMContentLoaded', function() {
  const togglers = document.querySelectorAll('[data-toggle-target]');

  Array.prototype.forEach.call(togglers, function(element) {
    const {
      toggleTarget: target,
      toggleVisibleClass: visibleClass,
      toggleFocusTarget: focusTarget,
      toggleBlurTarget: blurTarget
    } = element.dataset;
    toggler = Toggler({
      element,
      visibleClass,
      target: document.querySelector(target),
      focusTarget: focusTarget && document.querySelector(focusTarget),
      blurTarget: blurTarget && document.querySelector(blurTarget)
    });
    toggler.activate();
  });
});

export default () => toggler;
