const orderOptionsClassStart = 'js-order-options-';

document.addEventListener('DOMContentLoaded', function() {
  const pricingTable = document.getElementById('pricing-table');
  if (pricingTable) {
    const orderOptionsIndices = getOrderOptions(pricingTable);
    const orderOptionElements = document.querySelectorAll(
      '#order-form select option'
    );
    const buttons = pricingTable.querySelectorAll(
      '.et_pb_pricing_table_button'
    );
    Array.prototype.forEach.call(buttons, function(button, index) {
      button.addEventListener('click', function() {
        const orderOptionIndex = orderOptionsIndices[index];
        Array.prototype.forEach.call(orderOptionElements, function(
          element,
          index
        ) {
          if (index === orderOptionIndex) {
            element.setAttribute('selected', true);
          } else {
            element.removeAttribute('selected');
          }
        });
      });
    });
  }
});

const getOrderOptions = pricingTable =>
  Array.prototype.find
    .call(pricingTable.classList, className =>
      className.startsWith(orderOptionsClassStart)
    )
    .slice(orderOptionsClassStart.length)
    .split('-')
    .map(option => option - 1);
