window.addEventListener('load', function() {
  Array.prototype.forEach.call(
    document.getElementsByClassName('wpcf7-form'),
    function(form) {
      form.removeAttribute('novalidate');
    }
  );
});
