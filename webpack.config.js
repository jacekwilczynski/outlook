module.exports = require('./getWebpackConfig')({
  styles: { style: './sass/index.scss' },
  scripts: { script: './js/index.js' },
  alias: {
    theme: './sass/~theme',
    utils: './sass/~utils'
  },
  externals: {
    'jquery': 'jQuery'
  }
});
